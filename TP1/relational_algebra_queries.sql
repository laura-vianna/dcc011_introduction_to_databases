-- question 12
SELECT P.person_name, R.role_name
FROM person AS P
NATURAL JOIN role as R
NATURAL JOIN movie as M
WHERE M.movie_name='Back to the Future'

-- question 13
SELECT M.movie_name, P.person_name, MG.genre_name
FROM movie AS M
NATURAL JOIN role AS R
NATURAL JOIN person AS P
NATURAL JOIN movie_info AS MI
NATURAL JOIN movie_genre AS MG
WHERE MG.genre_name='Horror'
AND R.role_name='director'

-- question 14
SELECT M.movie_name, R.role_name, M.rating
FROM role AS R
NATURAL JOIN person AS P
NATURAL JOIN movie AS M
WHERE P.person_name='Foster, Jodie'

-- question 15
SELECT P.person_name, M.movie_name, M.rating
FROM person AS P
NATURAL JOIN movie AS M
NATURAL JOIN movie_info AS MI
NATURAL JOIN role AS R
WHERE MI.movie_genre_id=7
AND R.role_name='director'
ORDER BY M.rating DESC
