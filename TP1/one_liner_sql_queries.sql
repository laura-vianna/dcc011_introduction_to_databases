-- question 1.
SELECT M.movie_name, P.person_name FROM movie AS M JOIN role AS R ON R.movie_id = M.movie_id JOIN person AS P ON R.person_id = P.person_id WHERE R.role_name = 'director' AND P.gender='f'

-- question 2
SELECT COUNT(distinct R.movie_id) AS movie_count, COUNT(distinct R.role_name) AS role_count, P.person_name FROM person AS P NATURAL JOIN role AS R NATURAL JOIN role_type AS RT NATURAL JOIN movie_info AS MI NATURAL JOIN movie_genre AS MG WHERE (RT.type_name='actor' OR RT.type_name='actress') AND genre_name = 'crime' GROUP BY R.person_id ORDER BY role_count DESC LIMIT 10

-- question 5
SELECT M.movie_name, R.role_name FROM person AS P NATURAL JOIN role AS R NATURAL JOIN movie AS M NATURAL JOIN role_type AS RT WHERE P.person_name = 'Tarantino, Quentin' AND RT.type_name = 'actor'

-- question 6
SELECT movie_name, production_year, ranking FROM movie AS M WHERE M.movie_id IN (SELECT millenium_movies.movie_id FROM movie AS millenium_movies WHERE millenium_movies.production_year>2000 AND millenium_movies.ranking<=250) ORDER BY ranking LIMIT 20

-- question 7
SELECT RT.type_name, COUNT(DISTINCT M.movie_id) AS movies_amount FROM role AS R NATURAL JOIN movie M RIGHT JOIN role_type RT ON R.role_type_id = RT.role_type_id GROUP BY RT.type_name ORDER BY movies_amount DESC

-- question 8
SELECT AVG(M.rating) AS average_rating, MG.genre_name FROM movie_genre AS MG NATURAL JOIN movie_info AS MI NATURAL JOIN movie AS M GROUP BY MG.movie_genre_id ORDER BY average_rating DESC
