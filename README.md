# dcc011_introduction_to_databases  

## Authors/Autores:  
https://github.com/ArturHFS  
Eduardo  
https://gitlab.com/laura-vianna  

### Repository for coursework Introduction to Databases module - 2017/2  

1. TP1 - Queries for a IMDb database - SQL
2. TP2 - An User Interface for TED Talks queries - SQL, Ruby On Rails, Jupyter

#### TP2 Data files 
* dataset/ted_main.csv - Talks data  
* dataset/transcripts.csv - talks' transcripts   
* data_definition.sql - Database initialization script  
* events.csv - events data  
* parse_to_sql.ipynb - Jupyter notebook for generating ted_talks.sql from the data given  
* ted_talks.sql - SQL script for creating the database and inserting data  


#### What steps are necessary to get the application up and running.

Rails version: 5.1.4  
Ruby version: 2.4.2 (x86_64-linux)  

* System dependencies  
Installing Rbenv https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rbenv-on-ubuntu-16-04  

* Configuration
Install rbenv  
Install Ruby  
Install Ruby on Rails  

    * If database does not exist  
        Install mysql  
        https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-14-04  
        Import database  
    * If database already exists  
        Import database  

* Database version and initialization  
``mysql  Ver 14.14 Distrib 5.7.20, for Linux (x86_64) using  EditLine wrapper``  
``mysql < '$PATH/ibd_tp2/ted_talks.sql' -uroot -p``  

* How to run the application in local enviroment:  
`rails server`  
Open web browser  
Access http://0.0.0.0:3000  


### Repositorio para trabalho da disciplina Introducao a Banco de Dados - 2017/2

1. TP1 - Consultas no banco de dados IMDb - SQL
2. TP2 - Uma interface de usuario para consultas em um banco de dados TED Talks - SQL, Ruby On Rails, Jupyter

#### Arquivos de Dados do TP2
* dataset/ted_main.csv - Dados dos talks  
* dataset/transcripts.csv - Transcripts dos talks  
* data_definition.sql - Script de criação do banco, das tabelas e das chaves estrangeiras  
* events.csv - Dados sobre os eventos  
* parse_to_sql.ipynb - Notebook do Jupyter que cria o ted_talks.sql a partir dos dados  
* ted_talks.sql - Script sql que cria o banco e insere os dados  

#### Quais passos são necessários para configurar e executar a aplicação
 
Versão do Rails: 5.1.4  
Versão do Ruby: 2.4.2 (x86_64-linux)  

* Dependências do sistema  
Instalando o Rbenv https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-with-rbenv-on-ubuntu-16-04  

* Configuration
Instalar rbenv  
Instalar Ruby  
Instalar Ruby on Rails  

    * Se o Banco de Dados não existe:
        Instalar mysql  
        https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-14-04  
        Importar arquivo de dados   
    * Se o Banco de Dados existe:
        Importar arquivo de dados  

* Versão e inicialização do Banco de Dados  
``mysql  Ver 14.14 Distrib 5.7.20, for Linux (x86_64) using  EditLine wrapper``  
``mysql < '$PATH/ibd_tp2/ted_talks.sql' -uroot -p``  

* Como rodar a aplicação em ambiente local:  
`rails server`  
Abrir web browser  
Access http://0.0.0.0:3000  

