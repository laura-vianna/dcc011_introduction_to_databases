class Speaker < ActiveRecord::Base
  self.table_name = 'Speaker'
  self.primary_key = :speaker_id

  has_many :talks, class_name: 'Talk', foreign_key: :speaker_id
end
