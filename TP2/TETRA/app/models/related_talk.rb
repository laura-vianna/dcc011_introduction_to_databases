class RelatedTalk < ActiveRecord::Base
  self.table_name = 'Related_Talks'
  self.primary_key = %i[talk1_id talk2_id]

  belongs_to :talk, class_name: 'Talk', foreign_key: :talk1_id
  belongs_to :talk, class_name: 'Talk', foreign_key: :talk2_id
end
