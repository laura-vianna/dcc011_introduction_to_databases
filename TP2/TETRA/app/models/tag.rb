class Tag < ActiveRecord::Base
  self.table_name = 'Tag'
  self.primary_key = :tag_id

  has_many :talk_tags, class_name: 'TalkTag', foreign_key: :tag_id
end
