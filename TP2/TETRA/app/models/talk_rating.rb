class TalkRating < ActiveRecord::Base
  self.table_name = 'Talk_Rating'
  self.primary_key = %i[talk_id rating_id]

  belongs_to :rating, class_name: 'Rating', foreign_key: :rating_id
  belongs_to :talk, class_name: 'Talk', foreign_key: :talk_id
end
