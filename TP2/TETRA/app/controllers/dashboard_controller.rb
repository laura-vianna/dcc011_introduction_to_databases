class DashboardController < ApplicationController
    def index
        query_options
    end

    def query_options
        @query_options ||= Dir.entries("./app/assets/sql_queries/")
                              .reject { |f| File.directory? f }
                              .sort
    end
end
