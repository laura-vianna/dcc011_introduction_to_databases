class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    before_action :set_locale, :basic_queries, :report_queries, :transcript_queries

    def set_locale
        I18n.locale = params[:locale] || I18n.default_locale
    end

    def basic_queries
        # The queries from 01 to 10 are the basic, required queries
        @basic_queries ||= query_options[0..8]
    end

    def report_queries
        # The queries from 11 to 13 are part of advanced characteristic: report queries
        @report_queries ||= query_options[9..11]
    end

    def transcript_queries
        # The queries from 14 to 17 are part of advanced characteristic: text queries
        @transcript_queries ||= query_options[12..15]
    end

    private

    def query_options
        @query_options ||= Dir.entries('./app/assets/sql_queries/')
                              .reject { |f| File.directory? f }
                              .sort
    end
end
