class QueryController < ApplicationController
    def show
        set_query

        @query_result ||= ActiveRecord::Base.connection.select_all(set_query)
        @query_successful = @query_result.is_a? ActiveRecord::Result

        return unless @query_successful
        @html = syntax_query(@set_query)
        set_columns_and_rows
    end

    private

    def set_columns_and_rows
        @columns = @query_result.columns
        @rows = @query_result.rows
        @columns_num = @columns.size
        @rows_num = @rows.size
    end

    def syntax_query(code)
        CodeRay.scan(code, :sql).div(line_numbers: :table)
    end

    def coderay(text)
        text.gsub(/\<code( lang="(.+?)")?\>(.+?)\<\/code\>/m) do
            CodeRay.scan(Regexp.last_match(3), Regexp.last_match(2)).div(css: :class)
        end
    end

    def set_query
        @query_num ||= params[:id].to_s.rjust(2, '0')
        @set_query ||= IO.read("./app/assets/sql_queries/query_#{@query_num}.sql")
    end
end
