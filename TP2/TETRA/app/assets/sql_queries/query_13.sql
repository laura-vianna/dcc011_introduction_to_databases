SELECT E.country, COUNT(DISTINCT E.event_id) AS num_events, 
COUNT(DISTINCT T.talk_id) AS num_talks, COUNT(DISTINCT T.speaker_id) AS num_speaker, 
MIN(E.year) as first_year, MAX(T.views) as max_views 
FROM Event E NATURAL JOIN Talk T WHERE E.country IS NOT NULL
GROUP BY E.country 
ORDER BY num_events DESC;