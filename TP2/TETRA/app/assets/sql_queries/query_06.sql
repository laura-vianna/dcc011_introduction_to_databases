SELECT title, tag_name FROM Talk NATURAL JOIN Talk_Tag
NATURAL JOIN Tag NATURAL JOIN Event
WHERE `year` = 2008 AND tag_name = "entertainment"
ORDER  BY title;
