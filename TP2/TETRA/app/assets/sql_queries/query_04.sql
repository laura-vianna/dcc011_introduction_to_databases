SELECT year, COUNT(*) as num_talks FROM Talk NATURAL JOIN Event
WHERE year IS NOT NULL
GROUP BY year
ORDER BY num_talks DESC;
