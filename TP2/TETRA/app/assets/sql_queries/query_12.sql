SELECT event_name, COUNT(DISTINCT T.talk_id) AS num_talks, 
SUM(T.views) AS total_views, AVG(T.views) AS avg_views, 
COUNT(DISTINCT TG.tag_id) as num_tags, AVG(TR.count) as avg_rating, 
MIN(T.published_date) as first_talk 
FROM Event E NATURAL JOIN Talk T NATURAL JOIN Talk_Tag AS TT 
NATURAL JOIN Tag AS TG NATURAL JOIN Talk_Rating as TR 
GROUP BY event_id
ORDER BY num_talks DESC;