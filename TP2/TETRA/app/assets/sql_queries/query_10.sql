SELECT Rating.rating_name, AVG(Talk_Rating.count) AS average_count
FROM Rating NATURAL JOIN Talk_Rating NATURAL JOIN Talk
WHERE speaker_id IN
(
    SELECT speaker_id
    FROM Talk
    WHERE views < 150000
    ORDER BY views ASC
)
GROUP BY rating_name
ORDER BY average_count DESC;
