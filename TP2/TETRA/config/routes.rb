Rails.application.routes.draw do
    root to: 'dashboard#index'
    resources :query, only: [:show, :index]
end
