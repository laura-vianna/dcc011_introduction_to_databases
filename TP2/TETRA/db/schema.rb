# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  create_table "Event", primary_key: "event_id", id: :integer, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "event_name", limit: 100, null: false
    t.string "country", limit: 100
    t.integer "year"
  end

  create_table "Rating", primary_key: "rating_id", id: :integer, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "rating_name", limit: 100, null: false
    t.float "global_average", limit: 24
  end

  create_table "Related_Talks", primary_key: ["talk1_id", "talk2_id"], force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "talk1_id", null: false
    t.integer "talk2_id", null: false
    t.index ["talk2_id"], name: "fk_Related_Talks_talk2_id"
  end

  create_table "Speaker", primary_key: "speaker_id", id: :integer, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "speaker_name", limit: 100, null: false, collation: "utf8mb4_general_ci"
    t.string "occupation", limit: 100, collation: "utf8mb4_general_ci"
  end

  create_table "Tag", primary_key: "tag_id", id: :integer, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "tag_name", limit: 100, null: false
  end

  create_table "Talk", primary_key: "talk_id", id: :integer, default: nil, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "title", limit: 100, null: false
    t.integer "event_id", null: false
    t.integer "speaker_id", null: false
    t.text "description", collation: "utf8mb4_general_ci"
    t.integer "views"
    t.integer "comments"
    t.timestamp "published_date", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.text "transcript", collation: "utf8mb4_general_ci"
    t.index ["event_id"], name: "fk_Talk_event_id"
    t.index ["speaker_id"], name: "fk_Talk_speaker_id"
  end

  create_table "Talk_Rating", primary_key: ["talk_id", "rating_id"], force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "talk_id", null: false
    t.integer "rating_id", null: false
    t.integer "count", null: false
    t.index ["rating_id"], name: "fk_Talk_Rating_rating_id"
  end

  create_table "Talk_Tag", primary_key: ["talk_id", "tag_id"], force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "talk_id", null: false
    t.integer "tag_id", null: false
    t.index ["tag_id"], name: "fk_Talk_Tag_tag_id"
  end

  add_foreign_key "Related_Talks", "Talk", column: "talk1_id", primary_key: "talk_id", name: "fk_Related_Talks_talk1_id"
  add_foreign_key "Related_Talks", "Talk", column: "talk2_id", primary_key: "talk_id", name: "fk_Related_Talks_talk2_id"
  add_foreign_key "Talk", "Event", column: "event_id", primary_key: "event_id", name: "fk_Talk_event_id"
  add_foreign_key "Talk", "Speaker", column: "speaker_id", primary_key: "speaker_id", name: "fk_Talk_speaker_id"
  add_foreign_key "Talk_Rating", "Rating", column: "rating_id", primary_key: "rating_id", name: "fk_Talk_Rating_rating_id"
  add_foreign_key "Talk_Rating", "Talk", column: "talk_id", primary_key: "talk_id", name: "fk_Talk_Rating_talk_id"
  add_foreign_key "Talk_Tag", "Tag", column: "tag_id", primary_key: "tag_id", name: "fk_Talk_Tag_tag_id"
  add_foreign_key "Talk_Tag", "Talk", column: "talk_id", primary_key: "talk_id", name: "fk_Talk_Tag_talk_id"
end
